from django.contrib import admin

# Register your models here.
from .models import TempForm, PermForm
admin.site.register(TempForm)
admin.site.register(PermForm)