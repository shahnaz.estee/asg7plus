from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404, HttpResponseRedirect
from .forms import InputForm
from django.forms import ModelForm

# Create your views here.
def index(request):
    status = Status.objects.all()
    return render(request,'index.html', {'status': status})

def form(request):
    if request.method == "POST":
        form= InputForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('confirmation')
    else:
        form= InputForm()
    return render(request, 'form.html', {'temp-form': form})

def confirmation(request):
    if request.method == "POST":
        form = ConfirmForm(request.POST)
        if request.POST.get("confirm-button"):
            #add to confirm and delete
            return redirect('index')
        elif request.POST.get("cancel-button"):
            #just, delete
            return HttpResponseRedirect(reverse('form'))
    return render(request, 'confirmation.html', {'perm-form': form}, {'temp-form': temp})