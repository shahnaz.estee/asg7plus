from django.db import models

# Create your models here.
class TempForm(models.Model):
    name  = models.CharField(max_length=30)
    status = models.TextField()

class PermForm(models.Model):
    name = models.CharField(max_length=30)
    status = models.TextField()