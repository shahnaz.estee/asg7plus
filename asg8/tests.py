from django.test import LiveServerTestCase, TestCase
from django.utils import timezone
from selenium import webdriver

class AssignmentEightFunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.driver = webdriver.Chrome(
           chrome_options=chrome_options,
           executable_path='./chromedriver')

        self.driver.maximize_window()
        self.driver.get("https://assignment-seven-plus.herokuapp.com/")

    def tearDown(self):
        self.driver.quit()
        super().tearDown()

    def test_accordion(self):
        self.driver.get(self.live_server_url + '/faq/')

        self.assertIn('id="accordion-1"', self.driver.page_source)

        accordion = self.driver.find_elements_by_class_name('mb-0')[0]

        self.assertIs(accordion.get_attribtue("aria-expanded"), "false")
        accordion.click()
        self.assertIs(accordion.get_attribtue("aria-expanded"), "true")
