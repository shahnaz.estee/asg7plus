from django.test import LiveServerTestCase, TestCase
from django.utils import timezone
from selenium import webdriver

# Create your tests here.
class AssignmentSevenUnitTest(Testcase):
    def test_index_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    # def test_index_uses_index_template(self):
    #     response = Client().get('')
    #     self.assertTemplateUsed(response, 'index.html')

    # def test_index_uses_index_func(self):
    #     found = resolve('')
    #     self.assertEqual(found.func, index)

    def test_form_url_is_exist(self):
        response = Client().get('/form')
        self.assertEqual(response.status_code, 200)

    # def test_form_uses_form_template(self):
    #     response = Client().get('/form')
    #     self.assertTemplateUsed(response, 'form.html')

    # def test_form_uses_form_func(self):
    #     found = resolve('/form')
    #     self.assertEqual(found.func, form)

    def test_confirmation_url_is_exist(self):
        response = Client().get('/confirmation')
        self.assertEqual(response.status_code, 200)

    # def test_confirmation_uses_confirmation_template(self):
    #     response = Client().get('/confirmation')
    #     self.assertTemplateUsed(response, 'confirmation.html')

    # def test_confirmation_uses_confirmation_func(self):
    #     found = resolve('/confirmation')
    #     self.assertEqual(found.func, confirmation)

    # def test_create_status(self):
    #     Status.objects.create(name = "Shataso", date=timezone.now(), status = "testing")
    #     count = Status.objects.all().count()
    #     self.assertEqual(count, 1)
    
    # def test_confirmation_text_equals_written(self):
    #     @TODO

    # def test_can_save_a_POST(self):
    #     @TODO
    
# class AssignmentSevenFunctionalTest(LiveServerTestCase):
#     def setUp(self):
#         super().setUp()
#            chrome_options = webdriver.ChromeOptions()
#            chrome_options.add_argument('--no-sandbox')
#            chrome_options.add_argument('--headless')
#            chrome_options.add_argument('--disable-gpu')
#            self.driver = webdriver.Chrome(
#                chrome_options=chrome_options,
#                executable_path='./chromedriver')

#         self.driver.maximize_window()
#         self.driver.get("https://assignment-seven-plus.herokuapp.com/")

#     def tearDown(self):
#         self.driver.quit()
#         super().tearDown()
    
#     def test_can_post_and_look_at_status(self):
#         #Shataso opens this weird-looking faux social media web app
#         self.driver.get("https://assignment-seven-plus.herokuapp.com/")

#         #she looks at the home page and wants to post a status
#         updateStatusButton = driver.find_element_by_name("udpate-status-button")
#         @TODO

#         #she gets redirected to form input page and puts in a message
#         @TODO
#         nameField = driver.find_element_by_name("name-field")
#         nameField.send_keys("shataos")
#         textField = driver.find_element_by_name("text-field")
#         textField.send_keys("ttesitng :)")

#         #she presses the send button, and gets redirected to the confirmation page with the same text she put in
#         sendButton = driver.find_element_by_name("send-button")
#         @TODO

#         #she realized she had a typo, decided she wants to change it and presses no, and she gets redirected back to the form page
#         cancelButton = driver.find_element_by_name("cancel-button")
#         @TODO

#         #she puts a new message and gets redirected to confirmation page again, with the same text as the second try
#         nameField = driver.find_element_by_name("name-field")
#         nameField.send_keys("Shataso")
#         textField = driver.find_element_by_name("text-field")
#         textField.send_keys("testing :)")
#         @TODO

#         #she decided the message is good enough and said yes
#         confirmButton = driver.find_element_by_name("confirm-button")
#         @TODO

#         #she gets redirected back to the homepage and saw her post in there :D
#         @TODO
    

